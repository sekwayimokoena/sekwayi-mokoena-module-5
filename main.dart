// ignore_for_file: library_private_types_in_public_api, deprecated_member_use

//import 'dart:developer';
//import 'dart:ffi';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:two_developer/login.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:two_developer/viewUsers.dart';
import 'package:page_transition/page_transition.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
await Firebase.initializeApp(
  options: FirebaseOptions(
    apiKey: "AIzaSyC6mJsC7uBkB1Arz-yN2yCnjqgOcOUw7W4",
    authDomain: "skweb1st.firebaseapp.com",
    projectId: "skweb1st",
    storageBucket: "skweb1st.appspot.com",
    messagingSenderId: "73860968802",
    appId: "1:73860968802:web:cf6599b1aad7ccb5a7bc3b"
  )
);
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Unit Converter',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.white,
      ),
      home: const SplashScreen(),
    );
  }
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('images/Splash_screen.png'),
        ],
      ), 
      backgroundColor: Colors.black,
      nextScreen: const LoginScreen(),
      splashTransition: SplashTransition.fadeTransition,
      splashIconSize: 1500,
      pageTransitionType: PageTransitionType.fade,
      animationDuration: const Duration(seconds: 2),
      );
  }
}

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

    TextEditingController txtName= TextEditingController();
    TextEditingController txtSurname = TextEditingController();
    TextEditingController txtAddress = TextEditingController();
    TextEditingController txtCellphone = TextEditingController();

    @override
    Widget build(BuildContext context) {

      Future _addDetails(){
        final name = txtName.text;
        final surname = txtSurname.text;
        final address = txtAddress.text;
        final cellphone = txtCellphone.text;

        final ref = FirebaseFirestore.instance.collection("details").doc();
        return ref
        .set({
          "Name": name,
          "Surname": surname, 
          "Address": address,
          "Cellphone": cellphone,
          "doc_id:": ref.id,
        })
        .then((value) => 
          showDialog(
          context: context,
          builder: (_) => const AlertDialog(
          title: Text('New details saved!'),
          content: Text('Saved'),
        ),
        barrierDismissible: true,
          ),
        )
        .catchError((onError) => 
        showDialog(
          context: context,
          builder: (_) => const AlertDialog(
            title: Text('Something went wrong'),
            content: Text('Saved')
        ),
        ),
        );
      }

      return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: const Text('Manage user details'),
          backgroundColor: Colors.transparent,
          ),
        body:
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/backImage.jpg'),
                  fit: BoxFit.cover,
                  ),
            ),
            padding: const EdgeInsets.fromLTRB(50.0, 150.0, 50.0, 100.0),
            alignment: Alignment.center,
            child:
               Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: const <Widget>[
                      Text(
                      "Details",
                        style: TextStyle(fontSize:50.0,
                        color: Colors.grey,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Roboto"),
                      )
                    ]
                  ),
                  const SizedBox(
                      width: 300,
                      height: 20,
                      ),
                  TextField(
                    autofocus: true,
                    controller: txtName,
                    keyboardType: TextInputType.emailAddress,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.account_box,),
                      border: OutlineInputBorder(),
                      hintText: "Name",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  TextField(
                    controller: txtSurname,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.account_box),
                      border: OutlineInputBorder(),
                      hintText: "Surname",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                  TextField(
                    controller: txtAddress,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.home),
                      border: OutlineInputBorder(),
                      hintText: "Address",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 10,
                    ),
                    TextField(
                    controller: txtCellphone,
                    decoration: const InputDecoration(
                      prefixIcon: Icon(Icons.phone),
                      border: OutlineInputBorder(),
                      hintText: "Cellphone",
                    ),
                  ),
                  const SizedBox(
                      width: 300,
                      height: 30,
                    ),
                  Opacity(opacity: 0.40,
                    child: SizedBox(
                      width: 200,
                        height: 30,
                      child: RaisedButton(key:null, onPressed: _addDetails,
                        color: const Color(0xFFe0e0e0),
                        child:
                          const Text(
                          "Save",
                            style: TextStyle(fontSize:20.0,
                            color: Color(0xFF000000),
                            fontWeight: FontWeight.w100,
                            fontFamily: "Roboto"),
                          ),
                        ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Opacity(opacity: 0.40,
                    child: SizedBox(
                      width: 200,
                        height: 30,
                      child: RaisedButton(key:null, onPressed: clearText,
                        color: const Color(0xFFe0e0e0),
                        child:
                          const Text(
                          "Clear",
                            style: TextStyle(fontSize:20.0,
                            color: Color(0xFF000000),
                            fontWeight: FontWeight.w100,
                            fontFamily: "Roboto"),
                          ),
                        ),
                    ),
                  ),
                    SizedBox(
                    height: 10,
                  ),
                  Opacity(opacity: 0.40,
                    child: SizedBox(
                      width: 200,
                        height: 30,
                      child: RaisedButton(key:null, onPressed: viewRecords,
                        color: const Color(0xFFe0e0e0),
                        child:
                          const Text(
                          "View records",
                            style: TextStyle(fontSize:20.0,
                            color: Color(0xFF000000),
                            fontWeight: FontWeight.w100,
                            fontFamily: "Roboto"),
                          ),
                        ),
                    ),
                  )],
              ),
          ),
    
      );
    }
    void buttonPressed(){
        showDialog(
        context: context,
        builder: (_) => const AlertDialog(
        title: Text('New details saved!'),
        content: Text('Saved'),
        ),
        barrierDismissible: true,
      );
      }
    void viewRecords(){
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) {
          return const ViewDetails();
        }),);   
    }

    void clearText(){
      txtName.text = "";
      txtSurname.text = "";
      txtAddress.text = "";
      txtCellphone.text = "";
      txtName.selection;
    }
  }
    
