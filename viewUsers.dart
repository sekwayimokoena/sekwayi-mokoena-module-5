// ignore_for_file: library_private_types_in_public_api, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:two_developer/detailsList.dart';
import 'package:two_developer/main.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'User details',
      home: const ViewDetails(),
    );
  }
}

class ViewDetails extends StatefulWidget {
  const ViewDetails({Key? key}) : super(key: key);
  @override
  _ViewDetailsState createState() => _ViewDetailsState();
}

class _ViewDetailsState extends State<ViewDetails> {

    TextEditingController txtEmail = TextEditingController();
    TextEditingController txtPassword = TextEditingController();

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: const Text('Records'),
          backgroundColor: Colors.transparent,
          ),
        body:
          Container(
            decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('images/backImage.jpg'),
                  fit: BoxFit.cover,
                  ),
            ),
            child:
               Column(
                 children: [
                   DetailList()
                 ],
              ),
          ),
    
      );
    }
    void buttonPressed(){
    }
    
}