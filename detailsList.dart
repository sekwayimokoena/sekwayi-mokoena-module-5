import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import "package:firebase_core/firebase_core.dart";

class DetailList extends StatefulWidget {
  const DetailList({Key? key}) : super(key: key);

  @override
  State<DetailList> createState() => _DetailListState();
}

class _DetailListState extends State<DetailList> {
  final Stream<QuerySnapshot> _userDetails = FirebaseFirestore.instance.collection('details').snapshots();
  @override
  Widget build(BuildContext context) {

    TextEditingController editName = TextEditingController();
    TextEditingController editSurname = TextEditingController();
    TextEditingController editAddress = TextEditingController();
    TextEditingController editCellphone = TextEditingController();

    void _delete(do_cid){
        FirebaseFirestore.instance
        .collection("details")
        .doc(do_cid)
        .delete()
        .then((value) => showDialog(
        context: context,
        builder: (_) => const AlertDialog(
        title: Text('Confirmation'),
        content: Text('Record deleted'),
        ),
        barrierDismissible: true,
      ));
    }

    void _update(data){

      var collection = FirebaseFirestore.instance.collection("details");
      
      editName.text = data["Name"];
      editSurname.text = data["Surname"];
      editAddress.text = data["Address"];
      editCellphone.text = data["Cellphone"];
      
      showDialog(
        context: context, 
        builder: (_) => AlertDialog(
          title: Text("Edit"),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: editName,
              ),
              TextField(
                controller: editSurname,
              ),
              TextField(
                controller: editAddress,
              ),
              TextField(
                controller: editCellphone,
              ),
              TextButton(
                onPressed: (){
                  collection.doc(data["doc_id:"])
                  .update({
                    "Name": editName.text,
                    "Cellphone": editCellphone.text
                  });
                  Navigator.pop(context);
                }, 
                child: Text("Update"))
            ],
          ),
        ));
    }

    return StreamBuilder(
      stream: _userDetails, 
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot<Object?>> snapshot) {
        if(snapshot.hasError){
          return Text('Something went wrong');
        }
        if(snapshot.connectionState == ConnectionState.waiting){
          return CircularProgressIndicator();
        }
        if(snapshot.hasData){
          return Row(
            children: [
              Expanded(
                child: SizedBox(
                  height: (MediaQuery.of(context).size.height),
                  width: (MediaQuery.of(context).size.height),
                  child: ListView(
                    children: snapshot.data!.docs
                        .map((DocumentSnapshot documentSnapshot) {
                          Map<String, dynamic> data = documentSnapshot.data()! as Map<String, dynamic>;
                          return Column(
                            children: [
                              Opacity(
                                opacity: 0.7,
                                child: Card(
                                  child: Column(
                                    children: [
                                      ListTile(
                                        leading: Icon(Icons.account_circle),
                                        title: Text(data['Name']),
                                        subtitle: Text(data['Cellphone']),
                                      ),
                                      ButtonTheme(
                                        child: ButtonBar(
                                          children: [
                                            OutlinedButton.icon(
                                              onPressed: (){
                                                _update(data);
                                              }, 
                                              icon: Icon(Icons.edit), 
                                              label: Text('Edit')),
                                              OutlinedButton.icon(
                                              onPressed: (){
                                                _delete(data["doc_id:"]);
                                              }, 
                                              icon: Icon(Icons.delete), 
                                              label: Text('Delete'))
                                          ],
                                        ))
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          );
                      }).toList(),
                  ),
                ))
            ],
          );
        }else{
          return (Text('No data'));
        }
      },
    );
  }
}